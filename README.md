# Calculator Backend

## Setup For Development

- Install node modules via `npm install`
- Start tests with `npm run test:watch`
- Start server locally with `npm run start:watch`

## Setup Otherwise

- Run `docker-compose up` to build and start the service in a docker
