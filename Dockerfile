from node:12 as deps

WORKDIR /app

COPY package.json package-lock.json .npmrc ./

RUN npm install --pure-lockfile && \
    rm -f .npmrc


FROM node:12-slim

WORKDIR /app

COPY --from=deps /app/node_modules /app/node_modules
COPY --from=deps /app/package.json /app/

COPY tsconfig.json .eslintrc.js ./
COPY src/ ./src/

RUN npm run build

CMD [ "bash" ]
