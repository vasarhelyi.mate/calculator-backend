export type OperationInput = {
  a: number;
  b: number;
};

export type OperationOutput = {
  result: number;
};
