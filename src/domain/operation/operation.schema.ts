export const operationQuerySchema = {
  type: 'object',
  required: ['a', 'b'],
  additionalProperties: false,
  properties: {
    a: { type: 'number' },
    b: { type: 'number' },
  },
};

export const operationResponseSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    result: { type: 'number' },
  },
};
