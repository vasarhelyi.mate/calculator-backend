import exactMath from 'exact-math';
import { CalculatorService } from './calculator.service';

export const exactCalculatorFactory = (): CalculatorService => ({
  addititon: (a: number, b: number): number => exactMath.add(a, b),
  subtraction: (a: number, b: number): number => exactMath.sub(a, b),
  division: (a: number, b: number): number => exactMath.div(a, b),
  multiplication: (a: number, b: number): number => exactMath.mul(a, b),
});
