import { exactCalculatorFactory } from './exact-calculator.service';

describe('ExactCalculator', () => {
  describe('addititon', () => {
    it('should add numbers', () => {
      const calculator = exactCalculatorFactory();

      const result = calculator.addititon(1, 2);

      expect(result).toEqual(3);
    });
    it('should add decimals', () => {
      const calculator = exactCalculatorFactory();

      const result = calculator.addititon(1.1112, 2.3008);

      expect(result).toEqual(3.412);
    });
  });
  describe('subtraction', () => {
    it('should subtract numbers', () => {
      const calculator = exactCalculatorFactory();

      const result = calculator.subtraction(18, 7);

      expect(result).toEqual(11);
    });
    it('should subtract numbers with negative', () => {
      const calculator = exactCalculatorFactory();

      const result = calculator.subtraction(1, 2);

      expect(result).toEqual(-1);
    });
  });

  describe('division', () => {
    it('should divide numbers', () => {
      const calculator = exactCalculatorFactory();

      const result = calculator.division(12, 5);

      expect(result).toEqual(2.4);
    });
  });
  describe('multiplication', () => {
    it('should multiply numbers', () => {
      const calculator = exactCalculatorFactory();

      const result = calculator.multiplication(6, 2);

      expect(result).toEqual(12);
    });
    it('should multiply decimals', () => {
      const calculator = exactCalculatorFactory();

      const result = calculator.multiplication(6.34, 2.34);

      expect(result).toEqual(14.8356);
    });
  });
});
