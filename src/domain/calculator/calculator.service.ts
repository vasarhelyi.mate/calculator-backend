export type CalculatorService = {
  addititon: (a: number, b: number) => number;
  subtraction: (a: number, b: number) => number;
  division: (a: number, b: number) => number;
  multiplication: (a: number, b: number) => number;
};
