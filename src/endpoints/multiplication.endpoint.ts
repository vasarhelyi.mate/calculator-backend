import { CalculatorService } from '../domain/calculator/calculator.service';
import { OperationInput, OperationOutput } from '../domain/operation/operation';
import { operationQuerySchema, operationResponseSchema } from '../domain/operation/operation.schema';
import { Endpoint, EndpointMethod } from '../framework/endpoint';

export const multiplicationEndpointFactory = ({
  calculatorService,
}: {
  calculatorService: CalculatorService;
}): Endpoint<{}, OperationInput, {}, OperationOutput> => ({
  method: EndpointMethod.GET,
  route: '/multiplication',
  schema: {
    summary: 'Multiplies two numbers',
    response: {
      200: operationResponseSchema,
    },
    query: operationQuerySchema,
  },
  handler: async (request) => {
    const { a, b } = request.query;
    const result = calculatorService.multiplication(a, b);
    return { status: 200, response: { result } };
  },
});
