import { CalculatorService } from '../domain/calculator/calculator.service';
import { OperationInput, OperationOutput } from '../domain/operation/operation';
import { operationQuerySchema, operationResponseSchema } from '../domain/operation/operation.schema';
import { Endpoint, EndpointMethod } from '../framework/endpoint';

export const additionEndpointFactory = ({
  calculatorService,
}: {
  calculatorService: CalculatorService;
}): Endpoint<{}, OperationInput, {}, OperationOutput> => ({
  method: EndpointMethod.GET,
  route: '/addition',
  schema: {
    summary: 'Adds two numbers',
    response: {
      200: operationResponseSchema,
    },
    query: operationQuerySchema,
  },
  handler: async (request) => {
    const { a, b } = request.query;
    const result = calculatorService.addititon(a, b);
    return { status: 200, response: { result } };
  },
});
