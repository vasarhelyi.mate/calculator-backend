import { CalculatorService } from '../domain/calculator/calculator.service';
import { OperationInput, OperationOutput } from '../domain/operation/operation';
import { operationQuerySchema, operationResponseSchema } from '../domain/operation/operation.schema';
import { Endpoint, EndpointMethod } from '../framework/endpoint';

export const subtractionEndpointFactory = ({
  calculatorService,
}: {
  calculatorService: CalculatorService;
}): Endpoint<{}, OperationInput, {}, OperationOutput> => ({
  method: EndpointMethod.GET,
  route: '/subtraction',
  schema: {
    summary: 'Subtracts two numbers',
    response: {
      200: operationResponseSchema,
    },
    query: operationQuerySchema,
  },
  handler: async (request) => {
    const { a, b } = request.query;
    const result = calculatorService.subtraction(a, b);
    return { status: 200, response: { result } };
  },
});
