import { start } from './app';
import { config } from './config';

const thrower = (err: unknown): void => {
  throw err;
};

const throwToGlobal = (err: unknown): NodeJS.Immediate => setImmediate(() => thrower(err));

const startApp = async (): Promise<void> => {
  await start({ config });
};

startApp().catch(throwToGlobal);
