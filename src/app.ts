import { ConfigObject } from './config';
import { exactCalculatorFactory } from './domain/calculator/exact-calculator.service';
import { additionEndpointFactory } from './endpoints/addition.endpoint';
import { divisionEndpointFactory } from './endpoints/division.endpoint';
import { multiplicationEndpointFactory } from './endpoints/multiplication.endpoint';
import { subtractionEndpointFactory } from './endpoints/subtraction.endpoint';
import { Endpoint } from './framework/endpoint';
import { fastifyServerFactory } from './framework/fastify-server';

export const start = async ({ config }: { config: ConfigObject }): Promise<void> => {
  const calculatorService = exactCalculatorFactory();

  const additionEndpoint = additionEndpointFactory({ calculatorService });
  const subtractionEndpoint = subtractionEndpointFactory({ calculatorService });
  const divisionEndpoint = divisionEndpointFactory({ calculatorService });
  const multiplicationEndpoint = multiplicationEndpointFactory({
    calculatorService,
  });

  const endpoints = [additionEndpoint, subtractionEndpoint, divisionEndpoint, multiplicationEndpoint];

  const server = await fastifyServerFactory({
    endpoints: endpoints as Endpoint[],
  });

  await server.listen(config.port, config.host);
  console.info(`app listening on port ${config.host}:${config.port}`);
};
