import fastify, { RouteOptions, FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import fastifyCors from 'fastify-cors';
import http from 'http';
import { Endpoint } from './endpoint';
import { APIError } from './errors';

type FastifyFactoryArgs = {
  endpoints: Endpoint[];
};
export const fastifyServerFactory = async (factoryArgs: FastifyFactoryArgs): Promise<FastifyInstance> => {
  const fastifyServer = fastify<http.Server>({
    logger: false,
  });

  await fastifyServer.register(fastifyCors, {
    origin: '*',
  });

  const errorMapper = (err: unknown): { status: number; response: object } => {
    if (err instanceof APIError) {
      return {
        status: err.status,
        response: {
          detail: err.detail,
        },
      };
    }
    return {
      status: 500,
      response: { detail: err },
    };
  };

  const wrappedFastifyHandler = (endpointHandler: Endpoint['handler']) => async (
    request: FastifyRequest,
    reply: FastifyReply<http.Server>,
  ) => {
    try {
      const endpointArgs = {
        headers: request.headers as {},
        query: request.query as {},
        params: request.params as {},
        body: request.body as {},
      };
      const endpointResult = await endpointHandler(endpointArgs);
      return reply.status(endpointResult.status).send({ ...endpointResult.response });
    } catch (err) {
      const errorResponse = errorMapper(err);
      return reply.status(errorResponse.status).send({ ...errorResponse.response });
    }
  };

  factoryArgs.endpoints.forEach((endpoint) => {
    const fastifyRouteOptions: RouteOptions = {
      method: endpoint.method,
      url: endpoint.route,
      schema: endpoint.schema,
      handler: wrappedFastifyHandler(endpoint.handler),
    };
    fastifyServer.route(fastifyRouteOptions);
  });

  return fastifyServer;
};
