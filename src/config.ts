import convict from 'convict';
import * as dotenv from 'dotenv';

dotenv.config();
dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

export type ConfigObject = {
  domain: string;
  port: number;
  host: string;
  logger: {
    level: string;
  };
};

const convictConfig = convict<ConfigObject>({
  domain: {
    doc: 'Domain name.',
    format: String,
    default: 'http://localhost:3000',
    env: 'APP_DOMAIN',
  },
  port: {
    doc: 'The port to bind.',
    format: Number,
    default: 3000,
    env: 'PORT',
  },
  host: {
    doc: 'The host to serve on.',
    format: String,
    default: '0.0.0.0',
    env: 'HOST',
  },
  logger: {
    level: {
      doc: 'Defines the level of the logger.',
      format: String,
      default: 'debug',
      env: 'LOGGER_LEVEL',
    },
  },
});

convictConfig.validate({ allowed: 'strict' });

export const config = convictConfig.getProperties();
